package assignment.microsoft.com.movieapp.modules

import android.arch.persistence.room.Room
import android.content.Context
import assignment.microsoft.com.movieapp.db.MovieRoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ApplicationContextModule::class])
class MovieRoomDatabaseModule {

    @Provides
    @Singleton
    fun provideMovieRoomDatabase(context: Context): MovieRoomDatabase {
        return Room.databaseBuilder(context.applicationContext,
                MovieRoomDatabase::class.java, "movies_database")
                .build()
    }
}