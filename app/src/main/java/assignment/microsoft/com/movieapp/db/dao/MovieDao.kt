package assignment.microsoft.com.movieapp.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import assignment.microsoft.com.movieapp.db.entity.Movie

@Dao
interface MovieDao {
    @Insert
    fun insertListMovie(movie: List<Movie>)

    @Query("DELETE FROM movie_table")
    fun deleteAll()

    @Query("SELECT * from movie_table")
    fun getAllMovies(): List<Movie>
}