package assignment.microsoft.com.movieapp.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import assignment.microsoft.com.movieapp.app.MovieApplication
import assignment.microsoft.com.movieapp.model.MoviesInfo
import assignment.microsoft.com.movieapp.repository.BaseRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class MoviesViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        private val TAG: String = MoviesViewModel::class.java.simpleName
    }

    private var mMoviesListLiveData: MutableLiveData<List<MoviesInfo>>? = null
    private var mDisposable: CompositeDisposable? = null
    private var mRepository: BaseRepository

    init {
        mMoviesListLiveData = MutableLiveData()
        mDisposable = CompositeDisposable()
        mRepository = MovieApplication.INSTANCE.movieRepository
    }

    fun getAllMovies(): LiveData<List<MoviesInfo>> {
        mDisposable?.add(mRepository.getAllMovies()
                .subscribeOn(Schedulers.io())
                .map<List<MoviesInfo>> { movies ->
                    val moviesInfoList = ArrayList<MoviesInfo>()
                    for (movie in movies) {
                        val moviesInfo = MoviesInfo(movie.title, movie.getPopularity()!!, movie.getPosterPath()!!, movie.getOverview()!!)
                        moviesInfoList.add(moviesInfo)
                    }
                    moviesInfoList
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ moviesInfoList -> mMoviesListLiveData?.setValue(moviesInfoList) }, { throwable ->
                    Log.d(TAG, "Error in getAllMovies: " + throwable.message)
                    mMoviesListLiveData?.setValue(null)
                }))
        return mMoviesListLiveData as LiveData<List<MoviesInfo>>
    }

    fun reverseMovieList(movieList: List<MoviesInfo>): LiveData<List<MoviesInfo>> {
        mDisposable?.add(Observable.fromCallable {
            Log.d(TAG, "Reversed !")
            return@fromCallable movieList.reversed()
        }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ moviesInfoList ->
                    Log.d(TAG, "movie List  reversed !")
                    mMoviesListLiveData?.setValue(moviesInfoList)
                }, {
                    Log.d(TAG, "error in reversing movie List !")
                    mMoviesListLiveData?.setValue(null)
                }))
        return mMoviesListLiveData as LiveData<List<MoviesInfo>>
    }

    fun clearCachedData() {
        mDisposable?.add(mRepository.clearDb()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { success ->
                    if (success!!) {
                        Log.d(TAG, "clearCachedData success !")
                    } else {
                        Log.d(TAG, "clearCachedData failed  !")
                    }
                })
    }

    override fun onCleared() {
        super.onCleared()
        if (mDisposable != null) {
            Log.d(TAG, "Disposable cleared from onCleared!")
            mDisposable?.clear()
            mDisposable = null
        }
    }
}