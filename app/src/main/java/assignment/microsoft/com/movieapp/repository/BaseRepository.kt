package assignment.microsoft.com.movieapp.repository

import assignment.microsoft.com.movieapp.db.entity.Movie
import io.reactivex.Observable
import io.reactivex.Single

interface BaseRepository {
    fun getAllMovies(): Observable<List<Movie>>
    fun clearDb() : Single<Boolean>
}