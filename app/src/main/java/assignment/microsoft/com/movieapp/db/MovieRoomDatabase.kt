package assignment.microsoft.com.movieapp.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import assignment.microsoft.com.movieapp.app.MovieApplication
import assignment.microsoft.com.movieapp.db.dao.MovieDao
import assignment.microsoft.com.movieapp.db.entity.Movie

@Database(entities = [Movie::class], version = 1, exportSchema = false)
abstract class MovieRoomDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao

    companion object {

        fun getDatabase(): MovieRoomDatabase {
            return MovieApplication.INSTANCE.movieRoomDatabase
        }
    }
}