package assignment.microsoft.com.movieapp.component

import assignment.microsoft.com.movieapp.app.MovieApplication
import assignment.microsoft.com.movieapp.modules.GlideModule
import assignment.microsoft.com.movieapp.modules.MovieRoomDatabaseModule
import assignment.microsoft.com.movieapp.modules.NetworkModule
import assignment.microsoft.com.movieapp.modules.RepositoryModule
import dagger.Component
import javax.inject.Singleton


@Component(modules = [GlideModule::class, MovieRoomDatabaseModule::class, NetworkModule::class, RepositoryModule::class])
@Singleton
interface ApplicationComponent {
    fun inject(application: MovieApplication)
}