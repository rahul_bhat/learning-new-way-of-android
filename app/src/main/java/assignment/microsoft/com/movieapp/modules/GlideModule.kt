package assignment.microsoft.com.movieapp.modules

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import dagger.Module
import dagger.Provides

@Module(includes = [ApplicationContextModule::class])
class GlideModule {

    @Provides
    fun provideGlideRequestModule(context: Context): RequestManager {
        return Glide.with(context)
    }
}