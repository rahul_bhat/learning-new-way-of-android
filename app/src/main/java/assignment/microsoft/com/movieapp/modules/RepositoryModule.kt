package assignment.microsoft.com.movieapp.modules

import assignment.microsoft.com.movieapp.repository.BaseRepository
import assignment.microsoft.com.movieapp.repository.MoviesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule{

    @Provides
    @Singleton
    fun provideRepository():BaseRepository{
        return MoviesRepository
    }
}