package assignment.microsoft.com.movieapp.listing

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import assignment.microsoft.com.movieapp.DashboardActivity
import assignment.microsoft.com.movieapp.R
import assignment.microsoft.com.movieapp.adapter.MoviesListAdapter
import assignment.microsoft.com.movieapp.viewmodel.MoviesViewModel
import kotlinx.android.synthetic.main.fragment_movies.view.*

class MoviesFragment : Fragment(), View.OnClickListener {


    companion object {
        const val MOVIES_FRAGMENT_TAG: String = "MoviesFragment"
    }

    private var mRootView: View? = null
    private var mMoviesListAdapter: MoviesListAdapter? = null
    private var mDashboardActivity: DashboardActivity? = null
    private var mProgressBar: ProgressBar? = null
    private var mMoviesViewModel: MoviesViewModel? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is DashboardActivity)
            mDashboardActivity = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_movies, container, false)
            val recyclerView = mRootView?.movies_recycler_view
            recyclerView?.layoutManager = LinearLayoutManager(activity)
            mMoviesListAdapter = MoviesListAdapter(mDashboardActivity as AppCompatActivity, ArrayList())
            recyclerView?.adapter = mMoviesListAdapter
            mMoviesViewModel = ViewModelProviders.of(this).get(MoviesViewModel::class.java)
            mProgressBar = mRootView?.progressBar
            mProgressBar?.visibility = View.VISIBLE
            mRootView?.fab_button?.setOnClickListener(this)
            val swipeRefreshLayout = mRootView?.swipeRefreshLayout
            swipeRefreshLayout?.setColorSchemeColors(Color.GRAY, Color.GREEN, Color.BLUE,
                    Color.RED, Color.CYAN)
            swipeRefreshLayout?.setDistanceToTriggerSync(20)
            swipeRefreshLayout?.setOnRefreshListener {
                swipeRefreshLayout.isRefreshing = true
                mMoviesViewModel?.reverseMovieList(mMoviesListAdapter?.getMoviesInfoList()!!)?.observe(this, Observer {
                    if (it != null && it.isNotEmpty()) {
                        mMoviesListAdapter?.setData(it)
                    }
                })
                swipeRefreshLayout.isRefreshing = false
            }
            mMoviesViewModel?.getAllMovies()?.observe(this, Observer {
                if (it != null) {
                    mProgressBar?.visibility = View.GONE
                    mMoviesListAdapter?.setData(it)
                } else {
                    mProgressBar?.visibility = View.GONE
                    Toast.makeText(mDashboardActivity?.applicationContext, R.string.error_msg, Toast.LENGTH_SHORT).show()
                }
            })

        }
        return mRootView
    }

    override fun onClick(v: View?) {
        if (v?.id == R.id.fab_button) {
            mMoviesViewModel?.clearCachedData()
        }
    }
}