package assignment.microsoft.com.movieapp.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationContextModule(context: Context) {

    private val mContext: Context = context

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return mContext
    }
}