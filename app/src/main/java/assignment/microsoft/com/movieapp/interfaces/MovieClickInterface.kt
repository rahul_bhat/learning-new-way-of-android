package assignment.microsoft.com.movieapp.interfaces

import assignment.microsoft.com.movieapp.model.MoviesInfo

interface MovieClickInterface {
    fun onMovieListClick(moviesInfo: MoviesInfo)
}