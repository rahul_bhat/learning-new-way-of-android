package assignment.microsoft.com.movieapp.repository

import android.util.Log
import assignment.microsoft.com.movieapp.app.MovieApplication
import assignment.microsoft.com.movieapp.db.MovieRoomDatabase
import assignment.microsoft.com.movieapp.db.entity.Movie
import assignment.microsoft.com.movieapp.model.MoviesResponse
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.Callable

object MoviesRepository : BaseRepository {
    private val TAG = MoviesRepository.javaClass.simpleName

    private fun getAllMoviesFromApi(): Single<MoviesResponse> {
        return MovieApplication.INSTANCE.moviesApi.getAllMovies()
    }

    fun insertListIntoDb(movies: List<Movie>) {
        MovieRoomDatabase.getDatabase().movieDao().insertListMovie(movies)
    }

    override fun clearDb(): Single<Boolean> {
        return Single.fromCallable(Callable {
            return@Callable try {
                MovieRoomDatabase.getDatabase().movieDao().deleteAll()
                Log.d(TAG, "DB cache cleared !")
                true
            } catch (e: Exception) {
                Log.d(TAG, "DB cache not cleared !" + e.message)
                false
            }
        })
    }

    private fun getAllMoviesFromDb(): Observable<List<Movie>> {
        return Observable.fromCallable { MovieRoomDatabase.getDatabase().movieDao().getAllMovies() }
    }

    override fun getAllMovies(): Observable<List<Movie>> {
        return getAllMoviesFromDb()
                .flatMap { movies: List<Movie> ->
                    if (movies.isNotEmpty()) {
                        Log.d(TAG, "Returning from DB. Size = " + movies.size)
                        return@flatMap Observable.just(movies)
                    } else {
                        return@flatMap getAllMoviesFromApi()
                                .map { moviesResponse ->
                                    Log.d(TAG, "Got movie data from API. Size = " + moviesResponse.moviesInfoList.size)
                                    val movieList = ArrayList<Movie>()
                                    for (moviesInfo in moviesResponse.moviesInfoList) {
                                        val movie = Movie(moviesInfo.title)
                                        movie.setPosterPath(moviesInfo.posterPath)
                                        movie.setPopularity(moviesInfo.popularity)
                                        movie.setOverview(moviesInfo.overview)
                                        movieList.add(movie)
                                    }
                                    Single.create(SingleOnSubscribe<Unit> {
                                        Log.d(TAG, "Inserting into DB from " + Thread.currentThread().name + " thread.")
                                        insertListIntoDb(movieList)
                                    }).subscribeOn(Schedulers.io())
                                            .subscribe({
                                                Log.d(TAG, "Inserted movies !")
                                            }, {
                                                Log.d(TAG, "Exception in Inserting movies to DB " + it.message)
                                            })
                                    return@map movieList
                                }.toObservable()
                    }

                }


    }
}