package assignment.microsoft.com.movieapp

import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import assignment.microsoft.com.movieapp.details.MovieDetailFragment
import assignment.microsoft.com.movieapp.interfaces.MovieClickInterface
import assignment.microsoft.com.movieapp.listing.MoviesFragment
import assignment.microsoft.com.movieapp.listing.MoviesFragment.Companion.MOVIES_FRAGMENT_TAG
import assignment.microsoft.com.movieapp.model.MoviesInfo
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity(), MovieClickInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, MoviesFragment(), MOVIES_FRAGMENT_TAG)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onMovieListClick(@NonNull moviesInfo: MoviesInfo) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, MovieDetailFragment.newInstance(moviesInfo), MOVIES_FRAGMENT_TAG)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (supportFragmentManager.backStackEntryCount == 0) finish()
    }
}