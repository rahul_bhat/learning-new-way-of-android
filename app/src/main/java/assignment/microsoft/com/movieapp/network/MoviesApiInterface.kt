package assignment.microsoft.com.movieapp.network

import assignment.microsoft.com.movieapp.model.MoviesResponse
import io.reactivex.Single
import retrofit2.http.GET

interface MoviesApiInterface {
    @GET("9miey")
    fun getAllMovies(): Single<MoviesResponse>
}