package assignment.microsoft.com.movieapp.model

import com.google.gson.annotations.SerializedName

data class MoviesInfo(
        var title: String,
        var popularity: String,
        @SerializedName("poster_path")
        var posterPath: String,
        var overview: String
)