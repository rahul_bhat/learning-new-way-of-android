package assignment.microsoft.com.movieapp.details

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import assignment.microsoft.com.movieapp.R
import assignment.microsoft.com.movieapp.app.MovieApplication
import assignment.microsoft.com.movieapp.model.MoviesInfo
import kotlinx.android.synthetic.main.fragment_movie_detail.view.*

class MovieDetailFragment : Fragment() {
    companion object {
        private const val MOVIE_OVERVIEW = "MOVIE_OVERVIEW"
        private const val MOVIE_TITLE = "MOVIE_TITLE"
        private const val MOVIE_ICON = "MOVIE_ICON"
        private const val MOVIE_POPULARITY = "MOVIE_POPULARITY"

        fun newInstance(moviesInfo: MoviesInfo): MovieDetailFragment {
            val movieDetailFragment = MovieDetailFragment()
            val args = Bundle()
            args.putString(MOVIE_OVERVIEW, moviesInfo.overview)
            args.putString(MOVIE_TITLE, moviesInfo.title)
            args.putString(MOVIE_ICON, moviesInfo.posterPath)
            args.putString(MOVIE_POPULARITY, moviesInfo.popularity)
            movieDetailFragment.arguments = args
            return movieDetailFragment
        }
    }

    private var mRootView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_movie_detail, container, false)
            mRootView!!.findViewById<TextView>(R.id.movie_title).text = arguments?.getString(MOVIE_TITLE)
            mRootView!!.findViewById<TextView>(R.id.movie_popularity).text = arguments?.getString(MOVIE_POPULARITY)
            mRootView!!.findViewById<TextView>(R.id.movie_overview).text = arguments?.getString(MOVIE_OVERVIEW)
            MovieApplication.INSTANCE.glideRequestManager
                    .load(arguments?.getString(MOVIE_ICON))
                    .into(mRootView!!.movie_icon)
        }
        return mRootView
    }
}