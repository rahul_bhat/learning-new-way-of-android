package assignment.microsoft.com.movieapp.model

import com.google.gson.annotations.SerializedName

/**
 * Created by rahul.bhat on 2/9/19.
 */

data class MoviesResponse (
        @SerializedName("movie_list")
        val moviesInfoList: List<MoviesInfo> = emptyList()
)