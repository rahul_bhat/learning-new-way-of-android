package assignment.microsoft.com.movieapp.adapter

import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import assignment.microsoft.com.movieapp.R
import assignment.microsoft.com.movieapp.app.MovieApplication
import assignment.microsoft.com.movieapp.interfaces.MovieClickInterface
import assignment.microsoft.com.movieapp.model.MoviesInfo

class MoviesListAdapter constructor(appCompatActivity: AppCompatActivity, @NonNull var mMoviesInfoList: List<MoviesInfo>) : RecyclerView.Adapter<MoviesListAdapter.ViewHolder>() {
    var mMovieClickInterface: MovieClickInterface? = null

    init {
        if (appCompatActivity is MovieClickInterface) {
            mMovieClickInterface = appCompatActivity
        }
    }

    fun setData(moviesInfos: List<MoviesInfo>) {
        mMoviesInfoList = moviesInfos
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
        val rootView: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.movie_list_row, viewGroup, false)
        return ViewHolder(rootView)
    }

    override fun getItemCount(): Int {
        return mMoviesInfoList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.movieTitle?.text = mMoviesInfoList[position].title
        viewHolder.moviePopularity?.text = mMoviesInfoList[position].popularity
        viewHolder.movieIcon?.let {
            MovieApplication.INSTANCE.glideRequestManager
                    .load(mMoviesInfoList[position].posterPath)
                    .into(it)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var movieIcon: ImageView? = null
        var movieTitle: TextView? = null
        var moviePopularity: TextView? = null

        init {
            movieIcon = itemView.findViewById(R.id.movie_image)
            movieTitle = itemView.findViewById(R.id.movie_title)
            moviePopularity = itemView.findViewById(R.id.movie_popularity)
            itemView.setOnClickListener { mMovieClickInterface?.onMovieListClick(mMoviesInfoList[adapterPosition]) }
        }
    }

    fun getMoviesInfoList(): List<MoviesInfo> {
        return mMoviesInfoList
    }
}