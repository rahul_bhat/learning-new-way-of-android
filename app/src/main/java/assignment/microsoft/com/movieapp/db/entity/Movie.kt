package assignment.microsoft.com.movieapp.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "movie_table")
data class Movie(@PrimaryKey
                 @ColumnInfo(name = "title")
                 val title: String) {

    @ColumnInfo(name = "popularity")
    private var popularity: String? = null
    @ColumnInfo(name = "poster_path")
    private var posterPath: String? = null
    @ColumnInfo(name = "overview")
    private var overview: String? = null

    fun getPopularity(): String? {
        return popularity
    }

    fun setPopularity(popularity: String) {
        this.popularity = popularity
    }

    fun getPosterPath(): String? {
        return posterPath
    }

    fun setPosterPath(posterPath: String) {
        this.posterPath = posterPath
    }

    fun getOverview(): String? {
        return overview
    }

    fun setOverview(overview: String) {
        this.overview = overview
    }

}