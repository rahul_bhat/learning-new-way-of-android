package assignment.microsoft.com.movieapp.app

import android.app.Application
import assignment.microsoft.com.movieapp.component.ApplicationComponent
import assignment.microsoft.com.movieapp.component.DaggerApplicationComponent
import assignment.microsoft.com.movieapp.db.MovieRoomDatabase
import assignment.microsoft.com.movieapp.modules.ApplicationContextModule
import assignment.microsoft.com.movieapp.network.MoviesApiInterface
import assignment.microsoft.com.movieapp.repository.BaseRepository
import com.bumptech.glide.RequestManager
import javax.inject.Inject

class MovieApplication : Application() {
    lateinit var component: ApplicationComponent
    @Inject
    lateinit var glideRequestManager: RequestManager
    @Inject
    lateinit var movieRoomDatabase: MovieRoomDatabase
    @Inject
    lateinit var moviesApi: MoviesApiInterface
    @Inject
    lateinit var movieRepository: BaseRepository

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        component = DaggerApplicationComponent.builder()
                .applicationContextModule(ApplicationContextModule(this)).build()
        component.inject(this)
    }

    companion object {
        lateinit var INSTANCE: MovieApplication
    }

}